import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <!--The content below is only a placeholder and can be replaced.-->

    <table class="wrapper">
      <tr class="header">
        <td>
          <div>
            <img src="https://i.ibb.co/Hp4NPwg/Logo-3.png" />
          </div>
        </td>
      </tr>
      <tr class="lyra-email-title">
        <td>
          <div>
            <h1>You have been invited to a Plug!</h1>
          </div>
        </td>
      </tr>
      <tr class="lyra-email-intro">
        <td>
          <p>
            <strong>Hi {{ firstName }},</strong><br />
            {{ company.companyName }} has invited you to for the plug Fashion
            Week 2019 for $5000
          </p>
        </td>
      </tr>
      <tr class="lyra-campaign-info">
        <td>
          <div class="lyra-campaign-info-wrapper">
            <h4>
              <img src="https://i.ibb.co/hsV6Kmn/company-description.png" alt="company-description" border="0" />
              Company Description
            </h4>
            <p>
              {{ company.description }}
            </p>
                        <h4>
              <img src="https://i.ibb.co/hsV6Kmn/company-description.png" alt="company-description" border="0" />
              Event Description
            </h4>
            <p>
              {{ company.description }}
            </p>
            <h4>
              <img
                src="https://i.ibb.co/bspxn8D/post-description.png"
                alt="post-description"
                border="0"
              />
              Post Description
            </h4>
            <p>
              {{ gig.description }}
            </p>
            <h4>
              <img src="https://i.ibb.co/NZJc47c/Checkmark.png" alt="Checkmark" border="0" />
              What To Do
            </h4>
            <p>
              {{ gig.whatToDo }}
            </p>
            <h4 style="color: red">
              <img
                src="https://i.ibb.co/jbRDkqt/what-Not-To-Do.png"
                alt="what-Not-To-Do"
                border="0"
              />
              What Not To Do
            </h4>
            <p>
              {{ gig.exclude }}
            </p>
                        <h4 style="color: red">
              <img
                src="https://i.ibb.co/jbRDkqt/what-Not-To-Do.png"
                alt="what-Not-To-Do"
                border="0"
              />
              Location
            </h4>
            <p>
              {{ gig.address }}
            </p>
            <div class="seperator"></div>
            <h5>We think this might inspire you</h5>
            <img
              src="{{ photo.url }}"
              width="100"
              height="100"
              style="border-radius: 10px; padding: 5px; object-fit: cover;"
              *ngFor="let photo of gig.themePhotos"
            />
          </div>
        </td>
      </tr>

      <tr class="lyra-email-cta">
        <td>
          <div>
            <a href="https://www.lyra-influence.com" target="_blank"
              >View in Lyra</a
            >
          </div>
        </td>
      </tr>
    </table>

    <table class="lyra-email-footer">
      <tr>
        <td>
          <div>
            <p>
              We love hearing from you! <br />Have any questions? drop us a
              message on
              <span>support@lyra-influence.com</span>
            </p>
          </div>
        </td>
      </tr>
      <tr class="social-media-links">
        <td>
          <div>
            <a target="_blank" href="https://www.facebook.com/lyrainfluence/">
              <img src="https://i.ibb.co/VgskJgf/fb.png" />
            </a>

            <a target="_blank" href="https://www.instagram.com/lyra_influence/">
              <img src="https://i.ibb.co/y8nxqLx/insta.png" />
            </a>

            <a target="_blank" href="https://twitter.com/lyra_influence">
              <img src="https://i.ibb.co/vdD0Z87/twi.png" />
            </a>
          </div>
        </td>
      </tr>
    </table>

    <table class="lyra-email-footer-links">
      <tr>
        <td>
          <a href="https://www.lyra-influence.com/sign-in" target="_blank"
            >Unsubscribe</a
          >
        </td>

        <td>
          <a href="https://www.lyra-influence.com" target="_blank"
            >www.lyra.com</a
          >
        </td>

        <td>
          <a
            href="https://www.lyra-influence.com/terms-and-conditions"
            target="_blank"
            >Terms of Service</a
          >
        </td>
      </tr>
    </table>

    <table class="lyra-email-copyright">
      <tr>
        <td>
          <p>
            Copyright © 2019 Lyra Inc. All rights reserved.
          </p>
        </td>
      </tr>
    </table>
  `,
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  firstName = 'Sarah';

  companyName = 'Lyra';

  company = {
    companyName: 'Lyra',
    description: 'Hello this is our company Lyra',
  };

  gig = {
    "_id": "5cfa64c5c989070004d725b6",
    "coordinates": [-96.8024883, 33.0957151],
    "companyId": "5bf436fbb2113e0004210408",
     "title": "yo",
      "description": "dfjsklsdjf",
      "startDate": "2019-06-08T05:00:00.000Z",
      "endDate": "2019-06-09T05:00:00.000Z",
      "address": "8700 Preston Rd, Plano, TX 75024, USA",
      "location": "yea", "themePhotos": [], "whatToDo": "jdsakldjakldjsal", "contactPersonName": "djskl", "contactPersonNumber": "3424324234", "displayPhoto": { "_id": "5cfa64c5c989070004d725b7", "url": "https://res.cloudinary.com/muse/image/upload/v1559913624/qy46c7ypxmlzg0fwzrpa.png", "publicId": "qy46c7ypxmlzg0fwzrpa", "createdAt": "2019-06-07T13:21:09.364Z", "updatedAt": "2019-06-07T13:21:09.364Z" }, "cart": [], "createdAt": "2019-06-07T13:21:09.364Z", "updatedAt": "2019-06-07T13:21:09.364Z", "__v": 0, "attendingCount": 0, "pendingCount": 0, "spent": 0
  };
  constructor() { }

  ngOnInit() { }
}
