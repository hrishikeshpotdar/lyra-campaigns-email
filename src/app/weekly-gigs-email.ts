
export function weeklyGigsEmail(influencerFirstName: string, company: any, amount: number, gig: any) {

  return `<html lang="en">

<head>
  <meta charset="utf-8">
  <title>LyraEmailTemplate</title>
  <base href="/">

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="favicon.ico">
</head>

<body style="background:#E5E5E5">
  <div _nghost-sc0 ng-version="7.2.15">
    <table
      style="background-color:#fff;border:none;border-radius:4px;border-spacing:0;box-shadow:0px 12px 24px rgba(0, 0, 0, 0.08);box-sizing:border-box;font-family:-apple-system, BlinkMacSystemFont, &apos;Segoe UI&apos;, Roboto, Oxygen,
    Ubuntu, Cantarell, &apos;Open Sans&apos;, &apos;Helvetica Neue&apos;, sans-serif;margin:48px auto;margin-bottom:24px;max-width:520px;padding:0">
      <tbody  style="vertical-align:top">
        <tr  style="border:none;margin:0;padding:0">
          <td  style="padding:0">
            <div
              style="border-bottom:1px solid #e5e5e5;padding: 30px 0;text-align:center;width:100%"><img
                 src="https://i.ibb.co/Hp4NPwg/Logo-3.png"></div>
          </td>
        </tr>
        <tr  style="border:none;margin:0;padding:0">
          <td  style="padding:0">
            <div  style="margin:40px 48px">
              <h1  style="font-size:28px;line-height:34px;text-align:center">You have been invited to a
                Gig!</h1>
            </div>
          </td>
        </tr>
        <tr  style="border:none;margin:0;padding:0">
          <td  style="padding:0">
            <p  style="font-size:17px;letter-spacing:-0.024em;line-height:24px;margin:0 48px"><strong
                >Hi ${influencerFirstName},</strong><br >
              ${company.companyName} has invited you to for the ${gig.title} for $${amount}</p>
          </td>
        </tr>
        <tr  style="border:none;margin:0;padding:0">
          <td  style="padding:0">
            <div  style="margin:24px 48px">
              <h4
                style="all:unset;color:#11b8ae;font-size:17px;font-weight:bold;letter-spacing:-0.024em;line-height:24px">
                <img  alt="post-description" border="0"
                  src="https://i.ibb.co/bspxn8D/post-description.png"> Company Description </h4>
              <p
                style="color:#000000;font-size:15px;letter-spacing:-0.016em;line-height:20px;margin:8px 0 24px;opacity:0.7">
                ${company.description} </p>
                              <h4
                style="all:unset;color:#11b8ae;font-size:17px;font-weight:bold;letter-spacing:-0.024em;line-height:24px">
                <img  alt="post-description" border="0"
                  src="https://i.ibb.co/bspxn8D/post-description.png"> Contact Person Name </h4>
              <p
                style="color:#000000;font-size:15px;letter-spacing:-0.016em;line-height:20px;margin:8px 0 24px;opacity:0.7">
                ${gig.contactPersonName} </p>
                                              <h4
                style="all:unset;color:#11b8ae;font-size:17px;font-weight:bold;letter-spacing:-0.024em;line-height:24px">
                <img  alt="post-description" border="0"
                  src="https://i.ibb.co/bspxn8D/post-description.png"> Contact Person Number </h4>
              <p
                style="color:#000000;font-size:15px;letter-spacing:-0.016em;line-height:20px;margin:8px 0 24px;opacity:0.7">
                ${gig.contactPersonNumber} </p>
              <h4
                style="all:unset;color:#11b8ae;font-size:17px;font-weight:bold;letter-spacing:-0.024em;line-height:24px">
                <img src="https://i.ibb.co/NZJc47c/Checkmark.png" alt="Checkmark" border="0" /> What To Do </h4>
              <p
                style="color:#000000;font-size:15px;letter-spacing:-0.016em;line-height:20px;margin:8px 0 24px;opacity:0.7">
                ${gig.whatToDo}</p>
              <h4
                style="all:unset;font-size:17px;font-weight:bold;letter-spacing:-0.024em;line-height:24px">
                <img src="https://i.ibb.co/DVWcgpZ/location.png" alt="location" border="0">
                Location </h4>
              <p
                style="color:#000000;font-size:15px;letter-spacing:-0.016em;line-height:20px;margin:8px 0 24px;opacity:0.7">
               ${gig.address} </p>
              <div
                style="background:#e6e6e6;display:block;height:1px;margin-bottom:0px;position:relative;width:100%">
              </div>
              <h5 > We think this might inspire you </h5>

                            ${gig.themePhotos.map(
    photo =>
      `<img  height="90" style="border-radius:10px;padding:5px;object-fit:cover" width="90" src="${photo.url}" />`
  )};
            </div>
          </td>
        </tr>
        <tr  style="border:none;margin:0;padding:0">
          <td  style="padding:0">
            <div  style="margin:0 48px 48px"><a  href="https://www.lyra-influence.com"
                target="_blank"
                style="background:#11b8ae;border-radius:24px;color:#fff;cursor:pointer;display:table;font-size:17px;font-weight:700;line-height:24px;padding:12px 0;text-align:center;transition:all 300ms ease-in-out;vertical-align:middle;width:100%">View
                in Lyra</a></div>
          </td>
        </tr>
      </tbody>
    </table>
    <table  style="border:none;border-spacing:0;margin:0 auto;max-width:520px;padding:0">
      <tbody >
        <tr  style="border:none;margin:0;padding:0">
          <td  style="padding:0">
            <div  style="width:100%">
              <p
                style="color:#808080;font-family:-apple-system, BlinkMacSystemFont, &apos;Segoe UI&apos;, Roboto, Oxygen,
    Ubuntu, Cantarell, &apos;Open Sans&apos;, &apos;Helvetica Neue&apos;, sans-serif;font-size:12px;line-height:16px;text-align:center">
                We love hearing from you! <br >Have any questions? drop us a message on <span
                   style="color:#11b8ae">support@lyra-influence.com</span></p>
            </div>
          </td>
        </tr>
        <tr  style="border:none;margin:0;padding:0">
          <td  style="padding:0">
            <div  style="margin:16px 0;text-align:center;width:100%"><a
                href="https://www.facebook.com/lyrainfluence/" target="_blank" style="margin:0 12px"><img
                  src="https://i.ibb.co/VgskJgf/fb.png"></a><a
                href="https://www.instagram.com/lyra_influence/" target="_blank" style="margin:0 12px"><img
                   src="https://i.ibb.co/y8nxqLx/insta.png"></a><a
                href="https://twitter.com/lyra_influence" target="_blank" style="margin:0 12px"><img
                  src="https://i.ibb.co/vdD0Z87/twi.png"></a></div>
          </td>
        </tr>
      </tbody>
    </table>
    <table  style="border:none;border-spacing:0;margin:0 auto;max-width:520px;padding:0">
      <tbody >
        <tr  style="border:none;margin:0;padding:0">
          <td  style="padding:0"><a  href="https://www.lyra-influence.com/unsubscribe"
              target="_blank"
              style="color:#808080;font-family:-apple-system, BlinkMacSystemFont, &apos;Segoe UI&apos;, Roboto, Oxygen,
    Ubuntu, Cantarell, &apos;Open Sans&apos;, &apos;Helvetica Neue&apos;, sans-serif;font-size:12px;line-height:16px;margin:0 32px">Unsubscribe</a>
          </td>
          <td  style="padding:0"><a  href="https://www.lyra-influence.com" target="_blank"
              style="color:#808080;font-family:-apple-system, BlinkMacSystemFont, &apos;Segoe UI&apos;, Roboto, Oxygen,
    Ubuntu, Cantarell, &apos;Open Sans&apos;, &apos;Helvetica Neue&apos;, sans-serif;font-size:12px;line-height:16px;margin:0 32px">www.lyra.com</a>
          </td>
          <td  style="padding:0"><a
              href="https://www.lyra-influence.com/terms-and-conditions" target="_blank"
              style="color:#808080;font-family:-apple-system, BlinkMacSystemFont, &apos;Segoe UI&apos;, Roboto, Oxygen,
    Ubuntu, Cantarell, &apos;Open Sans&apos;, &apos;Helvetica Neue&apos;, sans-serif;font-size:12px;line-height:16px;margin:0 32px">Terms of Service</a></td>
        </tr>
      </tbody>
    </table>
    <table  style="border:none;border-spacing:0;margin:0 auto;max-width:520px;padding:0">
      <tbody >
        <tr  style="border:none;margin:0;padding:0">
          <td  style="padding:0">
            <p
              style="color:#808080;font-family:-apple-system, BlinkMacSystemFont, &apos;Segoe UI&apos;, Roboto, Oxygen,
    Ubuntu, Cantarell, &apos;Open Sans&apos;, &apos;Helvetica Neue&apos;, sans-serif;font-size:12px;line-height:16px;margin:24px 0 48px">
              Copyright &#xA9; 2019 Lyra Inc. All rights reserved. </p>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</body>
</html>
`;
}
